Ext.define('Plugin.forms_crm.Panel', {
    extend: 'Ext.form.Panel',
    id: 'crmpanel',
    plain: true,
    border: 0,
    bodyPadding: 5,
    bodyStyle: 'background:none',
    defaults: {
        labelAlign: 'top'
    },
    defaultType: 'numberfield',
    fieldDefaults: {
        labelWidth: 100,
        anchor: '100%'
    },
    layout: 'form',

    constructor : function(newDeal) {
        let compItems = [
            {
                name: 'name',
                xtype: 'textfield',
                fieldLabel: _('Название')
            },{
                name: 'deal_emails',
                xtype: 'textfield',
                fieldLabel: _('Emails')
            },{
                name: 'id_user',
                fieldLabel: _('Владелец')
            },{
                name: 'tag_type',
                fieldLabel: _('Воронка')
            },{
                name: 'tag_state',
                fieldLabel: _('Статус')
            },{
                name: 'tag_channel',
                fieldLabel: _('Канал')
            }
        ];

        if (newDeal) {
            compItems.unshift(
                {
                    name: 'form_id',
                    xtype: 'combobox',
                    fieldLabel: _('Форма'),
                    store: {
                        model: Plugin.forms_crm.models.Forms,
                        autoLoad: true
                    },
                    valueField:'id',
                    queryMode: 'local',
                    displayField: 'name',
                    triggerAction: 'all',
                    emptyText: _('Выберите форму'),
                    editable: false
                }
            );
        }

        Ext.apply(this, {
            items: compItems
        });

        this.callParent();
    }
});