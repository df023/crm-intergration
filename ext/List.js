Ext.define('Plugin.forms_crm.List', {
    extend: 'Ext.grid.Panel',
    requires: [
        'Plugin.forms.Model', 'Plugin.forms_crm.models.Deals', 'Plugin.forms_crm.models.Forms'
    ],
    border: false,
    columns: [{
        text: 'ID',
        dataIndex: 'form_id',
        width: 50
    }, {
        text: _('Форма'),
        dataIndex: 'form_name',
        flex: 1
    }, {
        text: _('Назвавние сделки'),
        dataIndex: 'name',
        flex: 1
    }, {
        text: _('Владелец сделки'),
        dataIndex: 'id_user',
        flex: 1
    }],

    store: {
        model: 'Plugin.forms_crm.models.Deals',
        autoLoad: true,
        autoSync: true
    },

    edit: function (record, newDeal = false) {
        Ext.create('Plugin.forms_crm.Edit', {
            record: record,
            newDeal: newDeal,
            listeners: {
                scope: this,
                createdeal: function (r) {
                    this.getStore().add(r);
                }
            }
        });
    },

    initComponent: function () {
        this.addAction = Ext.create('Ext.Action', {
            iconCls: 'icon-new',
            text: Config.Lang.add,
            scope: this,
            handler: function () {
                this.edit(Ext.create('Plugin.forms_crm.models.Deals'), true);
            }
        });

        this.editAction = Ext.create('Ext.Action', {
            iconCls: 'icon-edit',
            text: _('Изменить'),
            disabled: true,
            scope: this,
            handler: function () {
                const record = this.getSelectionModel().getSelection()[0];
                if (record) {
                    this.edit(record);
                }
            }
        });

        this.deleteAction = Ext.create('Ext.Action', {
            iconCls: 'icon-delete',
            text: Config.Lang.remove,
            disabled: true,
            scope: this,
            handler: function () {
                Ext.MessageBox.confirm(Config.Lang.delete, Config.Lang.r_u_sure, function (button) {
                    if (button === 'yes') {
                        const record = this.getSelectionModel().getSelection()[0];
                        if (record) this.getStore().remove(record);
                    }
                }, this);
            }
        });

        Ext.apply(this, {
            dockedItems: [{
                xtype: 'toolbar',
                items: [{
                    tooltip: Config.Lang.reload,
                    iconCls: 'icon-reload',
                    handler: function (btn) {
                        btn.up('grid').getStore().load();
                    }
                }, '-',
                    this.addAction,
                    this.editAction,
                    this.deleteAction
                ]
            }],

            viewConfig: {
                stripeRows: true,
                listeners: {
                    itemcontextmenu: {
                        scope: this,
                        fn: function (view, rec, node, index, e) {
                            e.stopEvent();
                            this.contextMenu.showAt(e.getXY());
                            return false;
                        }
                    }
                }
            }
        });

        this.contextMenu = Ext.create('Ext.menu.Menu', {
            items: [this.editAction, this.deleteAction]
        });

        this.getSelectionModel().on({
            scope: this,
            selectionchange: function (sm, selections) {
                if (selections.length) {
                    this.editAction.enable();
                    this.deleteAction.enable();
                } else {
                    this.editAction.disable();
                    this.deleteAction.disable();
                }
            },
        });

        this.on({
            'celldblclick': function () {
                this.edit(this.getSelectionModel().getSelection()[0]);
            },
            scope: this
        });

        this.callParent(arguments);
    }
});