Ext.define('Plugin.forms_crm.models.Deals', {
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'form_id'
        },{
            name: 'form_name',
        },{
            name: 'name',
            defaultValue: '[email]'
        },{
            name: 'id_user',
            type: 'integer',
            defaultValue: 214
        },{
            name: 'deal_emails',
            defaultValue: '[email]'
        },{
            name: 'estimate',
            type: 'integer',
            defaultValue: 1000
        },{
            name: 'tag_type',
            type: 'integer',
            defaultValue: 2224
        },{
            name: 'tag_state',
            type: 'integer',
            defaultValue: 2227
        },{
            name: 'tag_channel',
            type: 'integer',
            defaultValue: 2593
        }
    ],
    proxy: {
        type: 'ajax',
        api: {
            create  : '../plugins/forms_crm/data.php?action=create',
            read    : '../plugins/forms_crm/data.php',
            update  : '../plugins/forms_crm/data.php?action=update',
            destroy : '../plugins/forms_crm/data.php?action=delete'
        },
        reader: {
            type: 'json',
            root: 'rows'
        }
    }
}); 