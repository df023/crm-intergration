Ext.define('Plugin.forms_crm.models.Forms', {
    extend: 'Ext.data.Model',
    fields: ['name'],
    proxy: {
        type: 'ajax',
        api: {
            read: '../plugins/forms_crm/data.php?action=get_forms',
        },
        reader: {
            type: 'json',
            root: 'rows'
        }
    }
});