Ext.define('Plugin.forms_crm.Edit', {
    extend:'Ext.Window',
    modal: true,
    autoShow: true,
    width: 478,
    layout: 'card',
    title: _('Настройки сделки'),

    initComponent: function(){
        this.form = Ext.create('Plugin.forms_crm.Panel',this.newDeal);
        this.form.loadRecord(this.record);

        Ext.apply(this, {
            items: this.form,

            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                ui: 'footer',
                layout: {
                    pack: 'center'
                },
                items: [{
                    minWidth: 80,
                    text: Config.Lang.ok,
                    scope: this,
                    handler: function() {
                        let f = this.form.getForm();
                        if (f.isValid()) {
                            f.updateRecord();
                            if (!f.getRecord().getId()) this.fireEvent('createdeal', f.getRecord());
                            this.close();
                        }
                    }
                },{
                    minWidth: 80,
                    text: Config.Lang.cancel,
                    scope: this,
                    handler: function() {
                        this.close();
                    }
                }]
            }]
        });

        this.callParent();
    }
});
