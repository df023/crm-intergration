<?php

namespace FormsCrm;

use Forms\Form;

class CeteraCRM extends \Cetera\DbObject
{
    private static $hr_mode = false;
    private static $api_key = AYE_HASH;
    const USER = 1;
    const USER_HR = 2;
    const DEAL_TYPE = 3;
    const DEAL_STATE = 4;
    const DEAL_SOURCE = 5;
    const DEAL_CHANNEL = 6;
    const HR_DEAL_CHANNEL = 7;

    public function __construct($deal_data = null)
    {
        if (isset($deal_data)) {
            $deal_data['form_name'] = parent::getDbConnection()
                ->fetchAssoc('SELECT name FROM forms WHERE id = ?',
                    array($deal_data['form_id'])
                )['name'];
        }
        parent::__construct($deal_data);
    }

    public static function getTable()
    {
        return 'forms_crm';
    }

    public static function getById($id)
    {
        try {
            return parent::getById($id);
        } catch (\Exception $e) {
            throw new \Exception("CRM настройки для формы с id = $id не найдены");
        }
    }

    public static function getByFormId($id)
    {
        $data = parent::getDbConnection()->fetchAssoc('
                  	SELECT * 
                  	FROM ' . self::getTable() . '
                  	WHERE form_id = ?',
            array($id)
        );
        return $data;
    }

    public static function getList()
    {
        $data = [];

        foreach (self::enum() as $f) {
            $data[] = $f->fields;
        }
        return $data;
    }

    public static function getFormsWithoutDeal()
    {
        $data = parent::getDbConnection()->fetchAll(
            'SELECT id, name from ' . Form::getTable() . ' WHERE id not in ' .
            '(SELECT form_id from ' . self::getTable() . ')'
        );
        return $data;
    }

    public static function createDeal($deal, $fio = null)
    {
        try {
            if (strstr($_SERVER['HTTP_HOST'], 'hr.cetera.ru')
                || $_SERVER['PHP_SELF'] == '/support/stats.php'
                || stristr($_SERVER['PHP_SELF'], '/support/invite/')) {
                self::$hr_mode = true;
                self::$api_key = '1488228';
            } else {
                self::$hr_mode = false;
            }

            $deal_id = 0;

            if (!$_POST["accept_dbl"]) {
                $params = [
                    'deal_emails' => $deal['deal_emails']
                ];

                $res = self::sendRequest('deals', 'get', $params);
                if ($res['success']) {
                    $deal_id = $res['response'][0]['id'];

                    if (self::$hr_mode) {
                        $params = [
                            'id' => $deal_id,
                            'auto_notes' => true,
                            'data' => json_encode([
                                'tag_type' => $deal['tag_type'],
                                'tag_state' => $deal['tag_state']
                            ])
                        ];
                        self::sendRequest('deals', 'update', $params);
                    }
                }
            }

            if ($deal_id == 0) {
                if ($_COOKIE["roistat_visit"]) {
                    $roistat_code = $_COOKIE["roistat_visit"];
                } else {
                    $roistat_code = self::$hr_mode ? 'on.test.de' : 'test.de';
                }

                $deal['deal_roistat'] = $roistat_code;
                $deal['description'] ?: '';

                if (self::$hr_mode) {
                    $deal['tag_channel'] = self::HR_DEAL_CHANNEL;
                    if ($fio !== null) {
                        $deal['candidate_name'] = $fio;
                    }
                }

                $params = [
                    'data' => json_encode($deal),
                    'auto_notes' => true
                ];
                $res = self::sendRequest('deals', 'create', $params);
                if (!$res['success']) {
                    return -2;
                } else {
                    $deal_id = $res['response']['insert_id'];
                }
            }
            return $deal_id;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public static function createNote($deal_id, $name, $description = '', $type = 3)
    {
        $params = [
            'deal_id' => (int)$deal_id,
            'data' => json_encode([
                'name' => $name,
                'description' => $description,
                'type_id' => $type,
                'done' => 1
            ])
        ];

        $res = self::sendRequest('notes', 'create', $params);
        if (!$res['success']) {
            return -2;
        } else {
            return $res['response']['insert_id'];
        }
    }

    private static function sendRequest($entity, $method, $params)
    {
        $curl = curl_init();
        if (self::$hr_mode) {
            $domain = 'on.test.de';
            $protocol = 'http://';
        } else {
            $domain = 'crm.test.de';
            $protocol = 'https://';
        }

        $api_url = $protocol . $domain . '/api/' . $entity . '/' . $method;

        curl_setopt($curl, CURLOPT_URL, $api_url);
        curl_setopt($curl, CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, true);

        $post = $params;
        $post['access_key'] = self::$api_key;
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);

        $res = curl_exec($curl);
        return json_decode($res, true);
    }
}
