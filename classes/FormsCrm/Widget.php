<?php

namespace FormsCrm;

use Forms\Form;

class Widget extends \Forms\Widget
{
    protected function _getHtml()
    {
        try {
            $form = Form::getById($this->getParam('form'));
            if (isset($_REQUEST['webform']) && $_REQUEST['webform'] == $form->id) {
                $deal_data = CeteraCRM::getByFormId($form->id);
                if (is_array($deal_data)) {
                    $form_email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
                    $deal_data['name'] = str_replace('[email]',
                        $form_email, $deal_data['name']);
                    $deal_data['deal_emails'] = str_replace('[email]',
                        $form_email, $deal_data['deal_emails']);
                    $deal_data['description'] = "Email: {$form_email}";

                    unset($deal_data['id'], $deal_data['form_id'], $deal_data['form_name'],
                        $deal_data['accept_duplicates']
                    );

                    $noteDesc = Form::replaceTemplateInValue($form->fields['mailBody']);
                    $noteDesc .= "\n\n";
                    foreach($_POST as $field => $value) {
                        $noteDesc .= "{$field}: {$value}\n";
                    }
                    $dealId = CeteraCRM::createDeal($deal_data);
                    echo $dealId;
                    $noteTitle = strstr($deal_data['name'], '@') ? $deal_data['name'] :
                        $form_email . ' ' . $deal_data['name'];
                    CeteraCRM::createNote($dealId, $noteTitle, $noteDesc);
                }
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return parent::_getHtml();
    }
}
