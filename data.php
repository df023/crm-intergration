<?php
include('common_bo.php');

use FormsCrm\CeteraCRM;

$data = [];

if (isset($_GET['action'])) {
    $d = json_decode(file_get_contents('php://input'), true);

    if ($_GET['action'] == 'delete') {
        $crm_deal = CeteraCRM::getById((int)$d['id']);
        $crm_deal->delete();
    }

    if ($_GET['action'] == 'create') {
        $crm_deal = new CeteraCRM($d);
        $crm_deal->save();
        $data[] = $crm_deal->fields;
    }

    if ($_GET['action'] == 'update') {
        $crm_deal = CeteraCRM::getById((int)$d['id']);
        $crm_deal->setFields($d);
        $crm_deal->save();
        $data[] = $crm_deal->fields;
    }

    if ($_GET['action'] == 'get_forms') {
        $data = FormsCrm\CeteraCRM::getFormsWithoutDeal();
    }
} else {
    $data = CeteraCRM::getList();
}

echo json_encode(array(
    'success' => true,
    'rows' => $data
));