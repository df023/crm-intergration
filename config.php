<?php

$t = $this->getTranslator();
$t->addTranslation(__DIR__.'/lang');

$this->registerWidget(array(
    'name'     => 'forms',
    'class'    => '\\FormsCrm\\Widget',
    'describ'  => $t->_('Веб форма'),
    'icon'     => 'icon.png',
    'ui'       => 'Plugin.forms.Widget',
));

if ( $this->getBo() && $this->getUser() && $this->getUser()->isAdmin() ) {
    $this->getBo()->addModule(array(
        'id'	     => 'formsCrm',
        'position' => MENU_SITE,
        'name' 	   => $t->_('CRM сделки для форм'),
        'icon'     => 'icon.png',
        'class'    => 'Plugin.forms_crm.List'
	));
}